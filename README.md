# Document Ontology

A human-readable ontology documentation is available here: [https://gitlab.unige.ch/addmin/doc-onto/-/wikis](https://gitlab.unige.ch/addmin/doc-onto/-/wikis).
